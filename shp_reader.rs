/// quick hack to get some test data as `&[u8]`
/// use like
/// ```
/// rustc shp_reader.rs
/// for f in $(ls data/ten.*); do ./shp_reader "$f"; done >> src/testdata.rs
/// ```
use std::env;
use std::fs::File;
use std::io::Read;

fn format_data(name: String, d: &[u8]) -> String {
    let name = name.to_uppercase().replace(".", "_").replace("/", "_").replace("\\", "_");
    format!("pub const {}: &[u8] = &{:?};", name, d)
}

fn main() {
    let args: Vec<_> = env::args().collect();
    if args.len() != 2 || args[1] == "-help" || args[1] == "--help" {
        eprintln!(
            "read a file as bytes and generate rust consts.\nusage: {} filename",
            args[0]
        );
        return;
    }
    let mut file = File::open(&args[1]).unwrap();
    let mut v: Vec<u8> = vec![];
    file.read_to_end(&mut v).unwrap();
    println!("{}", format_data(args[1].clone(), &v));
}
