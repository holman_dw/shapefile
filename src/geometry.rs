#![allow(unused)]


// TODO flesh this out. what constitutes a geometry?
pub trait Geometry {
    fn as_geom() -> ();
}

// TODO determine if XYZM or 3d variants are more correct
#[derive(Debug, Copy, Clone)]
pub enum Point {
    Point(f64, f64),
    PointZ(f64, f64, f64),
    PointM(f64, f64, f64),
    PointZM(f64, f64, f64, f64)
}

impl Geometry for Point {
    fn as_geom() {}
}

// impl Point {
//     fn to_bytes(&self) -> &[u8] {}
// }

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct BBox {
    pub xmin: f64,
    pub ymin: f64,
    pub xmax: f64,
    pub ymax: f64,
    pub zmin: f64,
    pub zmax: f64,
    pub mmin: f64,
    pub mmax: f64,
}

#[cfg(test)]
mod test {
    use super::*;
    fn foo() {
        assert!(true);
    }
}


#[derive(Debug, PartialEq, Clone, Copy)]
pub enum ShapeType {
    // Name, Code
    NullShape,   // 0
    Point,       // 1
    PolyLine,    // 3
    Polygon,     // 5
    MultiPoint,  // 8
    PointZ,      // 11
    PolyLineZ,   // 13
    PolygonZ,    // 15
    MultiPointZ, // 18
    PointM,      // 21
    PolyLineM,   // 23
    PolygonM,    // 25
    MultiPointM, // 28
    MultiPatch,  // 31
}

impl ShapeType {
    pub fn from_i32(n: i32) -> Self {
        match n {
            0 => ShapeType::NullShape,
            1 => ShapeType::Point,
            3 => ShapeType::PolyLine,
            5 => ShapeType::Polygon,
            8 => ShapeType::MultiPoint,
            11 => ShapeType::PointZ,
            13 => ShapeType::PolyLineZ,
            15 => ShapeType::PolygonZ,
            18 => ShapeType::MultiPointZ,
            21 => ShapeType::PointM,
            23 => ShapeType::PolyLineM,
            25 => ShapeType::PolygonM,
            28 => ShapeType::MultiPointM,
            31 => ShapeType::MultiPatch,
            _ => ShapeType::NullShape,
        }
    }
    pub fn as_i32(self) -> i32 {
        match self {
            ShapeType::NullShape => 0,
            ShapeType::Point => 1,
            ShapeType::PolyLine => 3,
            ShapeType::Polygon => 5,
            ShapeType::MultiPoint => 8,
            ShapeType::PointZ => 11,
            ShapeType::PolyLineZ => 13,
            ShapeType::PolygonZ => 15,
            ShapeType::MultiPointZ => 18,
            ShapeType::PointM => 21,
            ShapeType::PolyLineM => 23,
            ShapeType::PolygonM => 25,
            ShapeType::MultiPointM => 28,
            ShapeType::MultiPatch => 31,
        }
    }
}
