
#[macro_use]
extern crate nom;

pub mod shp;
pub mod geometry;

#[cfg(test)]
pub mod testdata;


#[cfg(test)]
mod tests {
    // use super::*;
}
