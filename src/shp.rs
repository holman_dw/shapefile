#![allow(unused)]
use nom::{IResult, be_i32, le_f64, le_i32};
use geometry::{BBox, Geometry, Point, ShapeType};

pub struct Shp<T>
where
    T: Geometry,
{
    shp_header: ShpHeader,
    shp_geoms: ShpGeoms<T>,
}

// Shapefile Header information technical specification from:
// // https://www.esri.com/library/whitepapers/pdfs/shapefile.pdf
// //   Byte     Field          Default    Type     Byte Order
// //   ------------------------------------------------------
// //   0        File Code      9994       i32      Big
// //   4        Unused         0          i32      Big
// //   8        Unused         0          i32      Big
// //   12       Unused         0          i32      Big
// //   16       Unused         0          i32      Big
// //   20       Unused         0          i32      Big
// //   24       File Length    n          i32      Big
// //   28       Version        1000       i32      Little
// //   32       Shape Type     0          i32      Little
// //   36       Xmin           0          f64      Little
// //   44       Ymin           0          f64      Little
// //   52       Xmax           0          f64      Little
// //   60       Ymax           0          f64      Little
// //   68       Zmin           0          f64      Little
// //   76       Zmax           0          f64      Little
// //   84       Mmin           0          f64      Little
// //   92       Mmax           0          f64      Little
// //   ------------------------------------------------------
#[allow(unused)]
#[derive(Debug, PartialEq, Clone, Copy)]
struct ShpHeader {
    file_length: i32,
    version: i32,
    shape_type: ShapeType,
    bbox: BBox,
}

impl ShpHeader {
    // Parse the raw bytes
    pub fn from_bytes(b: &[u8]) -> Result<Self, String> {
        if b.len() != 100 {
            return Err(format!("expected header of size 100, got {}", b.len()));
        }
        match parse_header(b) {
            IResult::Done(_, h) => Ok(h),
            IResult::Error(err) => Err(format!("invalid shape header {:?}", err)),
            IResult::Incomplete(n) => Err(format!("invalid input bytes: {:?}", n)),
        }
    }
}

// TODO get rustfmt to ignore this.
named!(
    parse_header<ShpHeader>,
    do_parse!(
        // ignore the file code (9994)  and 5 unused i32 values  for now
        take!(24) >> file_length: be_i32 >> version: le_i32 >> shape_type: le_i32 >> xmin: le_f64
            >> ymin: le_f64 >> xmax: le_f64 >> ymax: le_f64 >> zmin: le_f64 >> zmax: le_f64
            >> mmin: le_f64 >> mmax: le_f64 >> (ShpHeader {
            file_length,
            version,
            shape_type: ShapeType::from_i32(shape_type),
            bbox: BBox {
                xmin,
                ymin,
                xmax,
                ymax,
                zmin,
                zmax,
                mmin,
                mmax,
            },
        })
    )
);

#[derive(Debug, Clone, PartialEq)]
struct ShpGeoms<T>
where
    T: Geometry,
{
    pub data: Vec<T>,
}

impl<T> ShpGeoms<T>
where
    T: Geometry,
{
    pub fn len(&self) -> usize {
        return self.data.len();
    }
}

impl ShpGeoms<Point> {
    fn from_bytes(b: &[u8], shape_type: ShapeType) -> Result<ShpGeoms<Point>, String> {
        let len = b.len();
        let v = match shape_type {
            // Point
            ShapeType::Point => read_pts(b, len / 28)?,
            // PointZ
            ShapeType::PointZ => read_pts_z(b, len / 36)?,
            // PointM
            ShapeType::PointM => read_pts_m(b, len / 36)?,
            _ => return Err(format!("invalid shape type {:?}", shape_type)),
        };
        let res: Result<ShpGeoms<Point>, String> = Ok(ShpGeoms::<Point> { data: v });
        return res;
    }
}

// parses a 2d point (XY). ignore the header,
// record number, and size for now.
named!(
    parse_pt_2d<(f64, f64)>,
    do_parse!(take!(12) >> x: le_f64 >> y: le_f64 >> (x, y))
);

// parses a 3d point (XYZ or XYM). ignore the header,
// record number, and size for now.
// TODO should this verify the record length to make sure the point isn't XYZM?
// The spec says that PointZ _should_ be XYZM, but actual data isn't like this.
named!(
    parse_pt_3d<(f64, f64, f64)>,
    do_parse!(take!(12) >> x: le_f64 >> y: le_f64 >> z: le_f64 >> (x, y, z))
);

fn read_pts(bytes: &[u8], num_pts: usize) -> Result<Vec<Point>, String> {
    let mut v: Vec<Point> = Vec::with_capacity(num_pts);
    let recwidth: usize = 28;
    for i in 0..num_pts {
        let (start, stop) = (i * recwidth, i * recwidth + recwidth);
        let p = match parse_pt_2d(&bytes[start..stop]) {
            IResult::Done(_, pt) => Point::Point(pt.0, pt.1),
            IResult::Error(err) => return Err(format!("error parsing file: {:?}", err)),
            IResult::Incomplete(_) => return Err("incomplete".into()),
        };
        v.push(p);
    }
    Ok(v)
}

fn read_pts_z(bytes: &[u8], num_pts: usize) -> Result<Vec<Point>, String> {
    let mut v: Vec<Point> = Vec::with_capacity(num_pts);
    let recwidth: usize = 36;
    for i in 0..num_pts {
        let (start, stop) = (i * recwidth, i * recwidth + recwidth);
        let p = match parse_pt_3d(&bytes[start..stop]) {
            IResult::Done(_, pt) => Point::PointZ(pt.0, pt.1, pt.2),
            IResult::Error(err) => return Err(format!("error parsing file: {:?}", err)),
            IResult::Incomplete(_) => return Err("incomplete".into()),
        };
        v.push(p);
    }
    Ok(v)
}

fn read_pts_m(bytes: &[u8], num_pts: usize) -> Result<Vec<Point>, String> {
    let mut v: Vec<Point> = Vec::with_capacity(num_pts);
    let recwidth: usize = 36;
    for i in 0..num_pts {
        let (start, stop) = (i * recwidth, i * recwidth + recwidth);
        let p = match parse_pt_3d(&bytes[start..stop]) {
            IResult::Done(_, pt) => Point::PointM(pt.0, pt.1, pt.2),
            IResult::Error(err) => return Err(format!("error parsing file: {:?}", err)),
            IResult::Incomplete(_) => return Err("incomplete".into()),
        };
        v.push(p);
    }
    Ok(v)
}

#[cfg(test)]
mod tests {
    use super::*;
    use testdata::DATA_TEN_SHP;
    // TODO write tests that actually verify the results. ie actual XY coordinates
    #[test]
    fn parse_point_header() {
        let h = ShpHeader::from_bytes(&DATA_TEN_SHP[0..100]).unwrap();
        eprintln!("{:?}", h);
        let expected = ShpHeader {
            file_length: 190,
            version: 1000,
            shape_type: ShapeType::Point,
            bbox: BBox {
                xmin: -123.815918,
                ymin: 47.5759181,
                xmax: -122.1398636,
                ymax: 48.993735,
                zmin: 0.0,
                zmax: 0.0,
                mmin: 0.0,
                mmax: 0.0,
            },
        };

        assert_eq!(h, expected);
    }
    #[test]
    fn parse_point_geometries() {
        let geoms = ShpGeoms::from_bytes(&DATA_TEN_SHP[100..], ShapeType::Point).unwrap();
        assert_eq!(geoms.len(), 10);
    }
}
