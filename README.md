# Shapefile

This package aims to implement the shapefile specification
[found here](https://www.esri.com/library/whitepapers/pdfs/shapefile.pdf)
in a pure rust package. To date, only reading the header of a shapefile
is implemented.

# Building

During the pre-0.1.0 phase, `bin/main.rs` will be unstable and potentially
have hard coded paths to shapefile(s). Later on, this may be changed to a
legitamate command line application. 

Building and running are done with `cargo`: `cargo build` & `cargo run`.

# Contact and Contributing

Hit me up on twitter [@holman_dw](https://twitter.com/holman_dw)
to chat about this. If you're interested in contributing, contact me
and we can talk about what needs to be done.

---

# Milestones

### 0.1.0

* read shapefiles - all geometry types
* shp + dbf records joined together as one output
* geometry will be stored either as structs or some other library's geometry types

### 0.2.0

* create new shapefile - all geometry types
  * shp + dbf
* verify that other existing technologies can read these

### 0.3.0

* modify existing shapefiles - all geometry types
  * shp + dbf

### 0.4.0

* read index file
   * support queries based on index

### 0.5.0

* create/update index file

### 0.6.0

* improve performance + provide benchmarks

# TODO

* change `Result<_, &'static str>` to something more idiomatic?
* make `GeomCollection` correctly generic
* verify shapefile before attempting to read

